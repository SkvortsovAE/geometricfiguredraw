﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GeometricFigureDraw
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new ApplicationViewModel();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            TabItem ti = options.SelectedItem as TabItem;
            ApplicationViewModel context = (ApplicationViewModel)this.DataContext;
            switch (ti.Name)
            {
                case "Triangle":
                    Polygon tr = new Polygon
                    {
                        Points = context.triangle.GetFigurePoints(),
                        Stroke = Brushes.Black,
                        Fill = Brushes.LightBlue
                    };
                    drawField.Children.Add(tr);
                    break;
                case "Square":
                    Polygon sq = new Polygon
                    {
                        Points = context.square.GetFigurePoints(),
                        Stroke = Brushes.Black,
                        Fill = Brushes.LightBlue
                    };
                    drawField.Children.Add(sq);
                    break;
                case "Sphere":
                    //Код для добавления сферы на холст
                    break;
            }

        }
    }
}

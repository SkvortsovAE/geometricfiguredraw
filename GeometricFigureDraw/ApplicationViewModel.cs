﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using GeometricFigureDraw.Entities;

namespace GeometricFigureDraw
{
    public class ApplicationViewModel : INotifyPropertyChanged
    {
        public Triangle triangle = new Triangle();

        public Square square = new Square();

        public Triangle Triangle
        {
            get { return triangle; }
            set
            {
                triangle = value;
                OnPropertyChanged("Triangle");
            }
        }

        public Square Square
        {
            get { return square; }
            set
            {
                square = value;
                OnPropertyChanged("Square");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}

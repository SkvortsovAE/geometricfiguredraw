﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GeometricFigureDraw.Entities
{
    public class Square : Figure, INotifyPropertyChanged
    {
        public int[] x = new int[4];
        public int[] y = new int[4];
        public int X1
        {
            get { return x[0]; }
            set
            {
                x[0] = value;
                OnPropertyChanged("X1");
            }
        }
        public int X2
        {
            get { return x[1]; }
            set
            {
                x[1] = value;
                OnPropertyChanged("X2");
            }
        }
        public int X3
        {
            get { return x[2]; }
            set
            {
                x[2] = value;
                OnPropertyChanged("X3");
            }
        }
        public int X4
        {
            get { return x[3]; }
            set
            {
                x[3] = value;
                OnPropertyChanged("X4");
            }
        }
        public int Y1
        {
            get { return y[0]; }
            set
            {
                y[0] = value;
                OnPropertyChanged("Y1");
            }
        }
        public int Y2
        {
            get { return y[1]; }
            set
            {
                y[1] = value;
                OnPropertyChanged("Y2");
            }
        }
        public int Y3
        {
            get { return y[2]; }
            set
            {
                y[2] = value;
                OnPropertyChanged("Y3");
            }
        }
        public int Y4
        {
            get { return y[3]; }
            set
            {
                y[3] = value;
                OnPropertyChanged("Y4");
            }
        }
        public Square() : base("Треугольник")
        {

        }
        public override PointCollection GetFigurePoints()
        {
            PointCollection result = new PointCollection();
            for (int i = 0; i < 4; i++)
            {
                result.Add(new System.Windows.Point(x[i], y[i]));
            }
            return result;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}

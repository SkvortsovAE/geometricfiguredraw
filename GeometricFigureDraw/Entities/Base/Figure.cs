﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

namespace GeometricFigureDraw.Entities
{
    public abstract class Figure
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string Name { get; set; }
        public float Area { get; set; }
        public Figure(string Name)
        {
            this.Name = Name;
        }
        public abstract PointCollection GetFigurePoints();
    }
}
